const User = require("../models/user");
const Order = require("../models/order");
const Product = require("../models/product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// user registration
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		email : reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user,error) => {
		if (error){
			return false
		}else{
			return true
		}
	})
};
// user authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {

		if(result == null){

			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result) }

			} else {

				return false
				
			};

		};
	});
};

// creating order
module.exports.checkout = async (data) =>{

    let prices = 0;
    for (let i = 0; i < data.order.length; i++) {
        let subPrice =await Product.findById(data.order[i].productId).then(result =>{return result.price;});
        prices += subPrice * data.order[i].quantity;
    }

    if(!data.isAdmin){
        let newOrder = new Order({
            userId : data.userId,
            products : data.order,
            totalAmount : prices
        });

        console.log(newOrder);

        return newOrder.save().then((order,error) =>{
            if(error){
                return false
            }
            else {
                return true
            }
        })
    }
}


module.exports.getUserDetails =(reqParams) =>{
    return User.findById(reqParams.userId).then(result =>{
        result = {
            email: result.email,
            isAdmin: result.isAdmin,
            password: "hidden for security reasons"
        }
        return result;
    })
}

