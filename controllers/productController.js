const Product = require("../models/product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// create products

module.exports.createProduct =(data) =>{
    if (data.isAdmin){
    let newProduct = new Product({
        name: data.product.name,
        description: data.product.description,
        price: data.product.price
    }) 
    // Saves the created object to our database
    return newProduct.save().then((product,error)=>{
        if (error){
            return false
        }else {
            return true
        }
    })
    }
    
    let message = Promise.resolve("User must be an Admin to add a product");
    return message.then((value)=>{
        return value;
    })

}

module.exports.getAllProducts =() =>{
	return Product.find({}).then(result =>{
		return result;
	});
};

// get all products
module.exports.getAllActiveProducts =() =>{
	return Product.find({isActive : true}).then(result =>{
		return result;
	});
};

// retrieves specific product
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

// updating product
module.exports.updateProduct = (reqParams,reqBody) =>{
	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		isActive : reqBody.isActive
	};
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product,error) =>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
};

// archiving product
module.exports.archiveProduct = (reqParams, isAdmin) =>{
	if(isAdmin){
		let updatedActiveField = {
		isActive : false
	};
	return Product.findByIdAndUpdate(reqParams.productId, updatedActiveField).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});
	};
	let message = Promise.resolve("User must be an Admin to archive a product");
    return message.then((value)=>{
        return value;
    });

};


// activating product
module.exports.activateProduct = (reqParams, isAdmin) =>{
	if(isAdmin){
		let updatedActiveField = {
		isActive : true
	};
	return Product.findByIdAndUpdate(reqParams.productId, updatedActiveField).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});
	};
	let message = Promise.resolve("User must be an Admin to activate a product");
    return message.then((value)=>{
        return value;
    });

};