const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController")
const auth = require("../auth")

// create products 
router.post("/",auth.verify,(req,res) =>{

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.createProduct(data).then(resultFromController => res.send(resultFromController));
});

// retrieve all products
router.get("/all-products",(req,res) =>{
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// retrieve all active products
router.get("/active-products",(req,res) =>{
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

// Retrieving a specific product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Updating product Information
router.put("/:productId",auth.verify,(req,res) =>{
	productController.updateProduct(req.params,req.body).then(resultFromController => res.send(resultFromController));
});

// Archiving a product
router.put("/:productId/archive", auth.verify, (req, res) => {
	        let isAdmin= auth.decode(req.headers.authorization).isAdmin;
	productController.archiveProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});

// Activativing a product
router.put("/:productId/activate", auth.verify, (req, res) => {
	        let isAdmin= auth.decode(req.headers.authorization).isAdmin;
	productController.activateProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});


module.exports = router;
