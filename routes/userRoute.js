const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")
const auth = require("../auth")

// user registration
router.post("/register",(req,res) =>{
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// user authentication(login)
router.post("/login", (req,res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Create Order
router.post("/checkout", auth.verify, (req,res) => {
    const data = {
        order : req.body,
        userId: auth.decode(req.headers.authorization).id,

        isAdmin : auth.decode(req.headers.authorization).isAdmin
    };
    userController.checkout(data).then(resultFromController => res.send(resultFromController));
});


// Retrive Users Details
router.get("/:userId/userDetails", auth.verify, (req,res) => {
    userController.getUserDetails(req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router;