const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name :{
		type : String,
		required : [true , "product name is required."]
	},
	description : {
		type : String,
		required : [true , "please describe the product."]
	},
	price :{
		type : Number,
		required : [true , "please mention the price of product."]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	}
});

module.exports = mongoose.model("Product" ,  productSchema);