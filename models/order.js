const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId : {
		type : String,
		required : [true, "ObjectId is required"]
	},
	orderedProducts : [
		{
			productid :{
				type : String,
				required : [true,"ProductId is required"]
			},
			quantity :{
				type : Number,
				required: [true,"please mention the Quantity of products required"]
			}
		}
	],
	totalAmount : {
		type : Number,
		required : [true, "mention the total amount of products ordered."]
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	}
});

module.exports = mongoose.model("Order" ,  orderSchema);