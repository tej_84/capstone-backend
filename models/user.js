const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email :{
		type : String,
		required : [true , "email is required."]
	},
	password : {
		type : String,
		required : [true , "please enter the password."]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
});

module.exports = mongoose.model("User" ,  userSchema);