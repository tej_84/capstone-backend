const jwt = require("jsonwebtoken");
const secret = "ProductBookingAPI";

// token creation
module.exports.createAccessToken = (user) =>{
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin,
	};
	return jwt.sign(data , secret ,{});
};

// token verification
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	// Token received and is not undefined
	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);
		
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return res.send({ auth: "failed" });
			} else {
				next()
			}
		})

	} else {
		return res.send({ auth: "failed" });
	};
};

module.exports.decode = (token) => {

	// Token received and is not undefined
	if(typeof token !== "undefined"){

		// Retrieves only the token and removes the "Bearer " prefix
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err){

				return null;

			} else {

				// The "decode" method is used to obtain the information from the JWT
				// The "{complete:true}" option allows us to return additional information from the JWT token
				// Returns an object with access to the "payload" property which contains user information stored when the token was generated
				// The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
				return jwt.decode(token, { complete: true }).payload;
			};
		})
	
	// Token does not exist
	} else {

		return null
	}

}
